#include "webhook.hpp"

#include <userver/tracing/span.hpp>
#include <userver/formats/json/serialize.hpp>
#include <userver/http/content_type.hpp>

namespace telegram {

namespace {
  const std::string kRequestDataName = "__request_json";
  const std::string kResponseDataName = "__response_json";
  const std::string kSerializeJson = "serialize_json";
} // namespace

WebhookApi::WebhookApi(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context)
    : HttpHandlerBase(config, context)
    , AbstractApi()
    {}

std::string WebhookApi::HandleRequestThrow(
      const userver::server::http::HttpRequest& request,
      userver::server::request::RequestContext& context) const {
    try {
      const auto& request_json =
        context.GetData<const userver::formats::json::Value&>(kRequestDataName);
      auto reply = ProcessMessage(request_json);

      if (!reply) {
        return "";
      }

      auto& response = request.GetHttpResponse();
      response.SetContentType(
          userver::http::content_type::kApplicationJson);

      const auto& response_json = context.SetData<const userver::formats::json::Value>(
          kResponseDataName,
          std::move(*reply));

      const auto scope_time =
          userver::tracing::Span::CurrentSpan().CreateScopeTime(kSerializeJson);
      return userver::formats::json::ToString(response_json);
    } catch (const std::exception& e) {
      LOG_ERROR() << "Caught an exception: " << e.what();

      return "";
    }
}

void WebhookApi::ParseRequestData(
    const userver::server::http::HttpRequest& request, userver::server::request::RequestContext& context) const {
  userver::formats::json::Value request_json;
  try {
    if (!request.RequestBody().empty())
      request_json = userver::formats::json::FromString(request.RequestBody());
  } catch (const userver::formats::json::Exception& e) {
    throw userver::server::handlers::RequestParseError(
        InternalMessage{"Invalid JSON body"},
        ExternalBody{std::string("Invalid JSON body: ") + e.what()});
  }

  context.SetData<const userver::formats::json::Value>(kRequestDataName, std::move(request_json));
}

}; // namespace telegram