#include "abstract.hpp"

namespace telegram {

void AbstractApi::RegisterMessageProcessor(const std::string& name, MessageProcessor&& message_processor) {
    std::lock_guard<userver::engine::SharedMutex> lock(message_processors_mutex_);

    message_processors_.try_emplace(name, std::forward<MessageProcessor&&>(message_processor));
}

void AbstractApi::UnregisterMessageProcessor(const std::string& name) {
    std::lock_guard<userver::engine::SharedMutex> lock(message_processors_mutex_);

    message_processors_.erase(name);
}

std::optional<userver::formats::json::Value> AbstractApi::ProcessMessage(const userver::formats::json::Value& message) const {
    std::shared_lock<userver::engine::SharedMutex> lock(message_processors_mutex_);

    for (auto& [_, message_processor] : message_processors_) {
        auto result = message_processor(message);

        if (result) {
            return result;
        }
    }

    return std::nullopt;
}

}; // namespace telegram