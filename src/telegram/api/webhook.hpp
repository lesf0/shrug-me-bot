#pragma once

#include <userver/server/handlers/http_handler_base.hpp>

#include "abstract.hpp"

namespace telegram {

class WebhookApi final : public userver::server::handlers::HttpHandlerBase, public AbstractApi {
  public:
    WebhookApi(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context);

    std::string HandleRequestThrow(
      const userver::server::http::HttpRequest& request,
      userver::server::request::RequestContext&) const override;

  protected:
    void ParseRequestData(const userver::server::http::HttpRequest& request,
                        userver::server::request::RequestContext& context) const override;
};

}; // namespace telegram