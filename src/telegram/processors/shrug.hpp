#pragma once

#include "abstract.hpp"

namespace telegram {

class ShrugProcessor : public AbstractProcessor {
  public:
    static constexpr std::string_view kName = "shrug-processor";

    ShrugProcessor(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context);

  protected:
    virtual std::optional<userver::formats::json::Value> ProcessMessage(const userver::formats::json::Value&) const override;
};

}; // namespace telegram