#include "shrug.hpp"

#include <userver/components/component_context.hpp>
#include <userver/formats/parse/common_containers.hpp>
#include <userver/formats/json/value_builder.hpp>
#include <userver/crypto/hash.hpp>

namespace telegram {

ShrugProcessor::ShrugProcessor(
      const userver::components::ComponentConfig& config,
      const userver::components::ComponentContext& context)
    : AbstractProcessor(config, context, kName)
{
}

std::optional<userver::formats::json::Value> ShrugProcessor::ProcessMessage(const userver::formats::json::Value& data) const {
    try {
        const auto& inline_query = data["inline_query"];
        const auto query = inline_query["query"].As<std::string>();

        const auto result = query + R"( ¯\_(ツ)_/¯)";
        
        userver::formats::json::ValueBuilder response_builder;
        response_builder["method"] = "answerInlineQuery";
        response_builder["inline_query_id"] = inline_query["id"];
        
        auto response_array = response_builder["results"];
        
        {
            userver::formats::json::ValueBuilder item_builder;
            item_builder["type"] = "article";
            item_builder["id"] = userver::crypto::hash::Sha256(result);
            item_builder["title"] = result;
            item_builder["input_message_content"]["message_text"] = result;
            response_array.PushBack(std::move(item_builder));
        }

        return response_builder.ExtractValue();
    } catch (const std::exception&) {
        return std::nullopt;
    }
}

}; // namespace telegram